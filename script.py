import boto3, os, sys, logging, time
from boto3 import Session

session = Session()
region = "us-east-1"
client = session.client('ec2', region_name=region)
ssm_client = boto3.client('ssm', region_name=region)

# used to do dry_run
dry_run = os.environ.get('DRY_RUN')
print(f"Dry run = {dry_run}")

branch_name = os.environ.get('BRANCH_NAME')
customer_id = os.environ.get('CUSTOMER_ID')

# logger
def setup_logger(region):
    data = {'region': region}

    logger = logging.getLogger()
    for handler in logger.handlers:
        logger.removeHandler(handler)
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] [%(region)s] : %(message)s')
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger = logging.LoggerAdapter(logger, data)
    return logger

# initialize the logger 
logger = setup_logger(region)
instances = client.describe_instances(Filters = [
        {
            'Name': 'instance-state-name', 
            'Values': ['running']
        },
        {
            'Name': 'tag:vertica_service_identifier', 
            'Values': [{customer_id}]
        }
    ])

running_instance = []

if len(instances['Reservations']) == 0:
    logger.info(f"No Instances found!")
    exit()

for instance in instances['Reservations']:
    for i in instance['Instances']:
            running_instance.append(i['InstanceId'])
        
if dry_run == "True":
    logger.info(f"Executing commands on the following instances: {','.join(running_instance)}")
else:
    try:
        logger.warning(f"Executing commands on the following instances:  {','.join(running_instance)}")
        response = ssm_client.send_command(
                    InstanceIds=[r for r in running_instance],
                    DocumentName="AWS-RunShellScript",
                    Parameters={'commands': ["sudo yum update -y",
                                             "sudo yum install git -y",
                                             "cd /home/ec2-user",
                                             "git clone https://aniketpatel2_crest@bitbucket.org/aniketpatel2_crest/ssm_into_jumphost.git",
                                             "cd ssm_into_jumphost",
                                             f"sudo git checkout -b {branch_name}",
                                             "touch text.txt"]},)
        command_id = response['Command']['CommandId']
        logger.info(f"Command id: {command_id}")
        for instance in running_instance:
            time.sleep(3)
            command_invocation_result = ssm_client.get_command_invocation(CommandId=command_id, InstanceId=instance)   
            logger.info(f"Command Execution Status: {command_invocation_result['Status']}")
                
    except Exception as e:
        logger.error(f"Failed to execute commands on the following instances:{','.join(running_instance)} with error {e}!")