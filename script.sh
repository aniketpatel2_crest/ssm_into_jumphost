set -e
for filename in data/*.json; do
    printf "\n$filename\n"
    python3 -m json.tool < $filename 
done