# import boto3, os, sys, logging
# from boto3 import Session

# session = Session()
# region = "us-east-2"
# client = session.client('ec2', region_name=region)
# ssm_client = boto3.client('ssm', region_name=region)

# # used to do dry_run
# dry_run = os.environ.get('DRY_RUN')
# print(f"Dry run = {dry_run}")

# def setup_logger(region):
#     data = {'region': region}

#     logger = logging.getLogger()
#     for handler in logger.handlers:
#         logger.removeHandler(handler)
#     handler = logging.StreamHandler(sys.stdout)
#     formatter = logging.Formatter('%(asctime)s [%(levelname)s] [%(region)s] : %(message)s')
#     handler.setFormatter(formatter)
#     logger.setLevel(logging.INFO)
#     logger.addHandler(handler)

#     logger = logging.LoggerAdapter(logger, data)
#     return logger

# # ec2_regions = [region['RegionName'] for region in client.describe_regions()['Regions']]
 
# logger = setup_logger(region)
# instances = client.describe_instances(Filters = [
#         {
#             'Name': 'instance-state-name', 
#             'Values': ['running']
#         }
#     ])

# running_instance = []

# if len(instances['Reservations']) == 0:
#     logger.info(f"No Instances found!")
#     exit()

# for instance in instances['Reservations']:
#     for i in instance['Instances']:
#             running_instance.append(i['InstanceId'])
        
# if dry_run == "True":
#     logger.info(f"Executing commands on the following instances: {','.join(running_instance)}")
# else:
#     try:
#         logger.warning(f"Executing commands on the following instances:  {','.join(running_instance)}")
#         response = ssm_client.send_command(
#                     InstanceIds=[r for r in running_instance],
#                     DocumentName="AWS-RunShellScript",
#                     Parameters={'commands': ["sudo yum update -y",
#                                              "sudo amazon-linux-extras install -y lamp-mariadb10.2-php7.2 php7.2",
#                                              "sudo yum install -y httpd mariadb-server",
#                                              "sudo systemctl start httpd",
#                                              "sudo systemctl enable httpd",
#                                              "sudo systemctl is-enabled httpd"]},)
#         command_id = response['Command']['CommandId']
#         logger.info(f"Command id: {command_id}")
#     except Exception as e:
#         logger.error(f"Failed to execute commands on the following instances:{','.join(running_instance)} with error {e}!")

    

import boto3, os, sys, logging
from boto3 import Session


session = Session()
client = session.client('ec2', region_name="ap-south-1")
ssm_client = boto3.client('ssm', region_name="ap-south-1")

# used to do dry_run
dry_run = os.environ.get('DRY_RUN')
print(f"Dry run = {dry_run}")

def setup_logger(region):
    data = {'region': region}

    logger = logging.getLogger()
    for handler in logger.handlers:
        logger.removeHandler(handler)
    handler = logging.StreamHandler(sys.stdout)
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] [%(region)s] : %(message)s')
    handler.setFormatter(formatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(handler)

    logger = logging.LoggerAdapter(logger, data)
    return logger

ec2_regions = [region['RegionName'] for region in client.describe_regions()['Regions']]

for region in ec2_regions:

    logger = setup_logger(region)
    ssm_conn = boto3.client('ssm', region_name=region)
    conn = session.client('ec2',region_name=region)
    instances = conn.describe_instances(Filters = [
            {
                'Name': 'instance-state-name', 
                'Values': ['running']
            }
        ])

    running_instance = []
    if len(instances['Reservations']) == 0:
        logger.info(f"No Instances found!")
        continue

    for instance in instances['Reservations']:
        for i in instance['Instances']:
                running_instance.append(i['InstanceId'])
            

    if dry_run == "True":
        logger.info(f"Executing commands on the following instances: {','.join(running_instance)}")
    else:
        try:
            logger.warning(f"Executing commands on the following instances:  {','.join(running_instance)}")
            response = ssm_conn.send_command(
                        InstanceIds=[r for r in running_instance],
                        DocumentName="AWS-RunShellScript",
                        Parameters={'commands': ["cd /home/ec2-user",
                                                "touch file1.txt"]},)
            command_id = response['Command']['CommandId']
            logger.info(f"Command id: {command_id}")
        except Exception as e:
            logger.error(f"Failed to execute commands on the following instances:{','.join(running_instance)} with error {e}!")

    

